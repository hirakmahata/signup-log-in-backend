const express = require("express");
const router = express.Router();

const User = require("../models/User");
const Tweet = require("../models/Tweet");
const Follower = require("../models/Follower")
const {Op} = require("sequelize");
const { user } = require("../server/config");


router.post("/home", async(request,response,next) => {

    try{ 
        
        let user_id =request.body.user_id;

        let followerData = await Follower.findAll({
            where:{
                user_id : user_id
            }
        });
        

        const followerId = followerData.map(follower=>{
            return follower.follower_id;
        })


        const followerDetails = followerId.map((id)=>{
            return User.findAll({
                attributes:["id","firstName","lastName"],
                where:{
                    id: id
                },
                include:{
                    model: Tweet,
                    required: true
                }
            })
        });

        const followers = await Promise.all(followerDetails);


        const tweetDetails = followers.map(follower=>{
            return {
                id: follower[0].id,
                name:follower[0].firstName +" "+follower[0].lastName,
                tweet: follower[0].tweets.reduce((acc,tweet)=>{
                    acc.push(tweet.tweet)
                    return acc;
                },[])
            }
        });


        let notFollowers = await User.findAll({
            where:{
                id: {
                    [Op.notIn] : [...followerId, parseInt(user_id)]
                }
            }
        })

        
        const notFollowerDetails = notFollowers.map(notFollower=>{
            return {
                id:notFollower.id,
                name: notFollower.firstName
            };
        })
        
        

        return response.json({
            follower_data : notFollowerDetails,
            follower_tweets: tweetDetails
        })

 }catch(err){
     console.error(err);
 }
 });
 
 
 module.exports = router;