const api_key = "76b65d96730811a088bf96b0dcd9b35c-28d78af2-e37fa8f5"
const domain = "sandbox1f9593f517cd4182b536045dd9cba716.mailgun.org"
const mailgun = require("mailgun-js")({ apiKey: api_key, domain: domain });


const express = require("express");
const User = require("../models/User.js");
const router = express.Router();
const bcrypt = require("bcrypt");



router.post("/signUp", async (request, response, next) => {

   try{

        let firstName = request.body.firstName;
        let lastName = request.body.lastName;
        let email = request.body.email;
        let password = request.body.password;


    if (firstName !== ""
        && lastName !== ""
        && email !== ""
        && password !== "") {
           

                let data = await User.findAll({
                    where:{
                        email : email
                    }
                });
                
                if (data.length == 0) {

                    let hashedPassword = await bcrypt.hash(password,10);
                    console.log(hashedPassword);
                    let OTP = Math.floor(Math.random() * (10000 - 1000) + 1000)

                    const data = {
                        from: 'Hirak Web <hirak.mahata@mountblue.tech>',
                        to: `${email}`,
                        subject: 'Verify your email',
                        html: "<b style='color:green'>Your OTP for login is: </b>"+ OTP
                    };

                    mailgun.messages().send(data, (error, body) => {
                        if (error) {
                            console.error(error);
                            
                        }
                
                    });

                    let user={
                        firstName,
                        lastName,
                        email,
                        password: hashedPassword ,
                        OTP
                    };
                
                    let insertedData= await User.create(user);

                    return response.json({
                        message: "registered successfully"
                    });

                       
                } else {
                    return response.json({
                        message: "email already exists"
                    })
                }


    } else {
        return response.json({
            message: "no field should be empty"
        })
    }
}catch(err){
        console.error(err);
    }

});



module.exports = router;