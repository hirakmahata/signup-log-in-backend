const express = require("express");
const router = express.Router();

const User = require("../models/User");
const Tweet = require("../models/Tweet");
const {Op} = require("sequelize");
const Follower = require("../models/Follower");


router.post("/follower", async(request,response,next) => {

    try{ 
        
        let followerName = request.body.followerName;
        let user_id =request.body.user_id;
        let follower_id = request.body.follower_id;

        let follow ={
            user_id,
            follower_id
        }

        let followData = await Follower.create(follow);

        let followerData = await User.findAll({
            where:{
                id : follower_id
            }
        });

        

        let name= followerData[0].firstName+" "+followerData[0].lastName;

        console.log(name);


        let tweetData = await Tweet.findAll({
            where:{
                user_id : follower_id
            }
        });

        tweetData.sort((a,b)=>{
            return (b.id)-(a.id);
        })

        let allTweets = tweetData.map(tweets=>{
            return tweets.tweet;
        })
         console.log(allTweets);

        return response.json({
            tweet: allTweets,
            name:name
        })

 }catch(err){
     console.error(err);
 }
 });
 
 
 module.exports = router;