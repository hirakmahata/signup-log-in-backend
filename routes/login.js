
const express = require("express");
const router = express.Router();
const bcrypt = require("bcrypt");


const User = require("../models/User");
const Follower = require("../models/Follower");
const Tweet = require("../models/Tweet");
const Likes = require("../models/Likes");
const Shares = require("../models/Shares");




router.post("/logIn", async (request, response, next) => {

   try{
    let email = request.body.email;
    let password = request.body.password;
    
    if (email !== "" && password !== "") {

        let data = await User.findAll({
            where:{
                email : email
            }
        })

                if (data.length == 0) {

                    return response.json({
                        message: "email does not exist. please sign up"
                    })
                } else if (!await bcrypt.compare(password, data[0].password)) {

                    return response.json({
                        message: "incorrect password. please enter correct password"
                    })
                } else {
                    return response.json({
                        user: data[0]
                    })
                }
            

    }else{
        return response.json({
            message: "email or password should not be empty"
        })
    }
}catch(err){
    console.error(err);
}


});


module.exports = router;