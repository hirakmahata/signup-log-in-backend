
const express = require("express");
const router = express.Router();

const Tweet = require("../models/Tweet");
const User = require("../models/User");



router.post("/tweet", async (request,response,next) => {
    try{
    let email = request.body.email;
    let tweet = request.body.tweet;

    if(tweet !== ""){

        let data = await User.findAll({
            where:{
                email : email
            }
        })

            let name = data[0].firstName +" "+ data[0].lastName;
            let user_id = data[0].id;

        let myTweet ={
            user_id,
            tweet
        }
        let insertedTweet= await Tweet.create(myTweet);

        let TweetData = await Tweet.findAll({
            where:{
                user_id : user_id
            }
        })
        TweetData.sort((a,b)=>{
            return (b.id)-(a.id);
        })

        return response.json({
            tweet : TweetData[0].tweet,
            name : name
        })

               
    }else{
        return response.json({
            message:"tweet can not be empty"
        })
    }
}catch(err){
    console.error(err);
}
});



module.exports = router;