const env_variable = require('./config.js');
const CORS = require("cors");
const express = require("express");
const path = require("path");
const signup = require("../routes/signup");
const otp = require("../routes/otp");
const login = require("../routes/login");
const home = require("../routes/home")
const tweet = require("../routes/tweet");
const follow = require("../routes/follower");



const sequelize = require("../server/connection");
const User = require("../models/User");
const Follower = require("../models/Follower");
const Tweet = require("../models/Tweet");
const Likes = require("../models/Likes");
const Shares = require("../models/Shares");




const app = express();
app.use(express.urlencoded({extended:true}))
app.use(express.json());
app.use(CORS());

app.use(signup);
app.use(otp);
app.use(login);
app.use(home);
app.use(tweet);
app.use(follow);


//associations

User.hasMany(Tweet,{foreignKey: "user_id"});
Tweet.belongsTo(User,{foreignKey: "user_id"});


app.use((req,res,next)=>{
    res.status(404).json({
      message : "404 not found."
    }).end();
  });



app.use((err, req, res, next)=>{
    console.error(err.stack);
    res.status(500).json({
      message: "internal server error"
    }).end();
  })
 
  
   sequelize.sync()
   .then(()=>{
    app.listen(env_variable.port,()=>{
      console.log(`server is listening on port: ${env_variable.port}......`);
    })
   })
   .catch(err=>{
     console.error(err);
   })
    

  
    