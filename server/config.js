const dotenv = require("dotenv");
const path = require('path');
dotenv.config({path : path.join(__dirname, "../.env")});


module.exports = {

    host : process.env.HOSTNAME,
    port : process.env.PORT,
    database : process.env.DATABASE,
    user : process.env.USERNAME,
    password : process.env.PASSWORD,
    dialect: "mysql",
    pool: {
      max: 5,
      min: 0,
      acquire: 30000,
      idle: 10000
    }

};