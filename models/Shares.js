const Sequelize = require("sequelize");
const sequelize = require("../server/connection");


const Shares = sequelize.define("shares", {
    id: {
        type: Sequelize.INTEGER,
        autoIncrement: true,
        primaryKey: true
    },
    user_id: {
        type: Sequelize.INTEGER,
        allowNull: false
    },
    tweet_id: {
        type: Sequelize.INTEGER,
        allowNull: false
    }
    
});

module.exports= Shares;