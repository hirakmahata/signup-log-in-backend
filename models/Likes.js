const Sequelize = require("sequelize");
const sequelize = require("../server/connection");


const Likes = sequelize.define("likes", {
    id: {
        type: Sequelize.INTEGER,
        autoIncrement: true,
        primaryKey: true
    },
    user_id: {
        type: Sequelize.INTEGER,
        allowNull: false
    },
    tweet_id: {
        type: Sequelize.INTEGER,
        allowNull: false
    }
    
});

module.exports= Likes;