const Sequelize = require("sequelize");
const sequelize = require("../server/connection")

const Follower = sequelize.define("follower", {
    id: {
        type: Sequelize.INTEGER,
        autoIncrement: true,
        primaryKey: true
    },
    user_id: {
        type: Sequelize.INTEGER,
        allowNull: false
    },
    follower_id: {
        type: Sequelize.INTEGER,
        allowNull: false
    },
    
});

module.exports= Follower;

