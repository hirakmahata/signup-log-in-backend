const Sequelize = require("sequelize");
const sequelize = require("../server/connection");

const Tweet = sequelize.define("tweet", {
    id: {
        type: Sequelize.INTEGER,
        autoIncrement: true,
        primaryKey: true
    },
    user_id: {
        type: Sequelize.INTEGER,
        allowNull: false
    },
     tweet : {
        type: Sequelize.STRING(1000),
        allowNull: false
    }
    
});

module.exports= Tweet;



